> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Carter Bass

### Assignment 2 Requirements:

*Three Parts:*

1. Tip Calculator App
2. Screenshots of the running App
3. Chapter Questions (Chapter 3)

#### README.md file should include the following items:

* Backward-engineer Tip Calculator App based on screenshots
* Include drop-down menu for total number of guests
* Drop-down menu for tip percentage (5% increments)
* Add a background color to the app
* Extra Credit - Display launcher icon

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of unpopulated App*:

![Tip Calculator Unpopulated](img/unpop.PNG)

*Screenshot of populated app*:

![Tip Calculator Populated](img/pop.PNG)

