> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Carter Bass

### Assignment 3 Requirements:

*Three Parts:*

1. Currency Converter App
2. Screenshots of the running App
3. Chapter Questions (Chapter 4)

#### README.md file should include the following items:

* Backward-engineer Tip Calculator App based on screenshots
* Field to enter U.S. dollar amount: 1-100000
* Must include toast notification if user enters out of range values
* Radio buttons for each currency conversion
* Must include correct sign for euros, pesos, and Canadian dollars
* Add a background color to the app
* Display launcher icon

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of unpopulated App*:

![Currency Calculator Unpopulated](img/first.PNG)

*Screenshot of toast notificaiton*:

![Toast Notificaiton](img/second.PNG)

*Screenshot of populated App*:

![Currency Calculator Populated](img/third.PNG)

