> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Carter Bass

### Project 2 Requirements:

*Three Parts:*

1. Database App
2. Screenshots of the running App
3. Chapter Questions

#### README.md file should include the following items:

* Backward-engineer Database App based on screenshots
* Insert at least five sample tasks
* Test database class
* Must add background color or theme
* Display launcher icon

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of main screen*:

![Main Screen](img/pic1.png)

