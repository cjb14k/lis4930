# LIS4930 - Advanced Mobile Application Development

## Carter Bass

### Assignment Links

*Assignment 1*
[A1 Repository Link](a1/README.md "Assignment 1")

    - Installation of Git
    - Installation of Android Studio
    - Installation of Java
    - Android Studio MyFirstApp
    - Android Studio Contacts App
    - git commands with definitions
    - Ch. 1 and Ch. 2 questions
    - Bitbucket introductory repositories  

*Assignment 2*
[A2 Repository Link](a2/README.md "Assignment 2")

    - Backward Engineer Tip Calculator App
    - Include drop-down box for number of guests
    - Include drop-down box for tip percentage
    - Include a different background color
    - Extra Credit for displaying a launcher icon
    - Ch. 3 Questions

*Assignment 3*
[A3 Repository Link](a3/README.md "Assignment 3")

    - Backward Engineer Tip Calculator App
    - Field to enter U.S. dollar amount: 1-100000
    - Toast notificaiton for out of range values
    - Radio buttons for each currency conversion
    - Includes correct sign for each currency
    - Change background color
    - Display launcher icon

*Project 1*
[P1 Repository Link](p1/README.md "Project 1")

    - Backward-engineer My Music App based on screenshots
    - Include splash screen image, app title, intro text.
    - Include artists' images and media.
    - Images and buttons must be vertically and horizontally aligned.
    - Must add background color or theme
    - Display launcher icon

*Assignment 4*
[A4 Repository Link](a4/README.md "Assignment 4")

    - Backward-engineer Mortgage Interest Calculator App based on screenshots
    - Include splash screen image, app title, intro text.
    - Include appropriate images.
    - Must use persistent data: SharedPreferences
    - Widgets and images must be vertically and horizontally aligned
    - Must add background color or theme
    - Display launcher icon

*Assignment 5*
[A5 Repository Link](a5/README.md "Assignment 5")

    - Backward-engineer RSS Feed App based on screenshots
    - Include splash screen with app title and list of articles.
    - Must find and use your own RSS Feed.
    - Must add background color or theme
    - Display launcher icon

*Project 2*
[P2 Repository Link](p2/README.md "Project 2")

    - Backward-engineer Database App based on screenshots
    - Insert at least five sample tasks
    - Test database class
    - Must add background color or theme
    - Display launcher icon