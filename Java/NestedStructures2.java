import java.util.Scanner;
import java.util.*;

public class NestedStructures2
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		List<Double> grades = new ArrayList<Double>();
		double input = 0.0;
		double total = 0.0;
		double average = 0.0;
		double count = 0.0;

		while (input >= 0 && input <= 100)
		{
			System.out.print("Enter Score: ");
			input = scan.nextDouble();
			if (input >= 0 && input <= 100)
			{
				grades.add(input);
			}
		}

		count = grades.size();
		System.out.printf("\nCount: " + "%.2f", count);
		for (int i=0; i<grades.size(); i++)
		{
			total += grades.get(i);
		}

		System.out.printf("\nTotal: " + "%.2f", total);
		average = total/count;
		System.out.printf("\nAverage: " + "%.2f", average);
	}
}