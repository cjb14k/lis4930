import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class EvenOrOdd
{
	private static class InstructionDisplay extends JPanel
	{
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			g.drawString("Program uses Java GUI message and input dialogs.", 20, 30);
			g.drawString("Program evaluates integers as even or odd.", 20, 40);
			g.drawString("Note: Program does *not* perform data validation.", 20, 50);
		}
	} 

	private static class ButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}

	public static void main(String[] args)
	{
		InstructionDisplay displayPanel = new InstructionDisplay();
		JButton okButton = new JButton("OK");
		ButtonHandler listener = new ButtonHandler();
		okButton.addActionListener(listener);



		JPanel content = new JPanel();
		content.setLayout(new BorderLayout());
		content.add(displayPanel, BorderLayout.CENTER);
		content.add(okButton, BorderLayout.SOUTH);

		JFrame frame = new JFrame("EvenOrOdd");
		frame.setContentPane(content);
		frame.setSize(400,200);
		frame.setLocation(400,400);
		frame.setVisible(true);







		//Scanner input = new Scanner(System.in);
		//int num = 0;

		//System.out.print("Enter integer: ");
		//num = input.nextInt();

		//if (num/2 == 0)
		//{
		//	System.out.println(num + " is an even number.");
		//}
		//else
		//{
		//	System.out.println(num + " is an odd number.");
		//}
	}
}