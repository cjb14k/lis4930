> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Carter Bass

### Project 1 Requirements:

*Three Parts:*

1. My Music App
2. Screenshots of the running App
3. Chapter Questions (Chapter 6)

#### README.md file should include the following items:

* Backward-engineer My Music App based on screenshots
* Include splash screen image, app title, intro text.
* Include artists' images and media.
* Images and buttons must be vertically and horizontally aligned.
* Must add background color or theme
* Display launcher icon

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Splash Screen*:

![Splash Screen](img/splash.PNG)

*Screenshot of main screen*:

![Main Screen](img/main.PNG)

*Screenshots of Play/Pause Buttons*:

![Slushii Pause](img/playSlushii.PNG)

![Kid Pause](img/playKid.PNG)

![Bonnie X Clyde Pause](img/playBonnie.PNG)

