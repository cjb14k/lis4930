> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Carter Bass

### Assignment 4 Requirements:

*Three Parts:*

1. Mortgage Interest Calculator App
2. Screenshots of the running App
3. Chapter Questions (Chapter 11)

#### README.md file should include the following items:

* Backward-engineer Mortgage Interest Calculator App based on screenshots
* Include splash screen image, app title, intro text.
* Include appropriate images.
* Must use persistent data: SharedPreferences
* Widgets and images must be vertically and horizontally aligned
* Must add background color or theme
* Display launcher icon

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Splash Screen*:

![Splash Screen](img/splash.PNG)

*Screenshot of main screen*:

![Main Screen](img/home.PNG)

*Screenshot of Invalid input*:

![Invalid](img/invalid.PNG)

*Screenshot of final input*

![Final](img/final.PNG)