> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930 - Mobile Web Application Development

## Carter Bass

### Assignment 5 Requirements:

*Three Parts:*

1. RSS Feed App
2. Screenshots of the running App
3. Chapter Questions (Chapter 6, 7)

#### README.md file should include the following items:

* Backward-engineer RSS Feed App based on screenshots
* Include splash screen with app title and list of articles.
* Must find and use your own RSS Feed.
* Must add background color or theme
* Display launcher icon

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Splash Screen*:

![Splash Screen](img/one.PNG)

*Screenshot of article screen*:

![Article Screen](img/two.PNG)

*Screenshot of Web page*:

![Web Page](img/three.PNG)

